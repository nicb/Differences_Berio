\version "2.22.0"

\header {
  title = "Différences -- Polarità"
  composer = "Luciano Berio -- Analisi Nicola Bernardini"
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}
\score {
  \new Staff {
    \relative c' {
    \clef alto
    \tempo 4 = 68
    \time 6/4
    d2.(_\ppp\upbow^\markup{IV}^\markup{ "(vla)" }~ d4\<^\markup{ "verso il pont." }^\markup{ "poco a poco" } d2^\markup{III}) |
    <d e>2.\!^\markup{ "ord."}\>~ <d e>8 r8\! r2 |
    r2 r4 d2.(^\markup{ "III" }~ |
    \time 2/4
    \tempo \markup { "accel." }
    d4\< d4)\!\p~ |
    \numericTimeSignature
    \time 4/4
    \tuplet 5/4 { d16\! r16 <d e>2\upbow\<~ } \tuplet 5/4 { <d e>4\! r16 } r4 |
    \clef bass
    \tempo 4 = 80
    \tuplet 3/2 { d4--\mf^\markup{ "(vc.)" } d8-- } \tuplet 3/2 { d8-- d4--\> } r2\! |
    \clef alto
    r2 des2->\f\downbow\>^\markup{ "(vla.)" } |
    r1\! |
    }
  }
  \layout {
    \context {
      \Score
      \override SpacingSpanner.common-shortest-duration =
        #(ly:make-moment 1/16)
    }
  }
  \midi {}
}

