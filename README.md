# Una introduzione a *Différences* di Luciano Berio (1959)

Speech (in italian) delivered:

1. during the SMC-Chigiana online seminars 12/05/2023


## LICENSE

[<img
  src=http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png
  width=88
  alt="CreativeCommons Attribution-ShareAlike 4.0"
/>](./LICENSE)
